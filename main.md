# Constitution of TSWR

We as a people have decided to unite under the governance of The Social Web Republic (TSWR). Above all will stand the constitution, as defined within this document.

[TOC]

## Section A: Definition of terms

1. A party is a group of at least 5 representatives that must have declared a valid political goal (plausibly attainable before or after election within the confines of the law) to the representative council, consist of at least one representative from each state, and run for election.
2. The executive governate (or governate) is defined under the constitution as the party-elect, infrastructural ministry, representative council, and legislative senate as a collective working as the central authority for TSWR.
3. The party-elect is the top level executive position and is a group of at least 5 party members representing the party holding the most seats. The group will consist of a party representative for each state, has the power to cast votes to the populous on constitutional reform, has the power to enact said constitutional reform only with majority vote from the populous, has the power to pass or reject laws with a majority vote from the party-elect representatives, and has the power to exercise executive authority with the support of a unanimous vote from the party-elect representatives.
4. The representative council (or RC) is a branch of the governate with the duty of providing a fair representation of the people's interest. The people may elect RC members to hold seats in the representative assembly.
   1. The representative assembly will consist of a governate appointed chairman, and 10 members from each state who will be elected by the state's population. It will be tasked with voting on whether RC members from the department of nomination will enter other government assemblies, and nominating for and voting on the removal of a member from an assembly.
   2. The department of election will consist of 3 assembly appointed representatives from each state. The department of election is tasked with the fair representation of the people's will through voting. It will count and cast votes and elections.
   3. The department of nomination will consist of citizens wishing to represent their state in a government assembly. As a member of the department of representation you are expected to appear before the representative assembly and present what department you wish to enter and why you should be accepted. To be accepted into a department you must receive a majority vote from the representative assembly.
5. The infrastructural ministry (or IM) is a branch of the governate with the duty of founding and organizing necessary government organizations and institutions.
   1. The infrastructural assembly will consist of a governate appointed chairman, and 3 members from each state who will be selected through the representative assembly. It will be tasked with the nomination and election of representatives to and removal from the seats within the departments of the IM. It will also vote on actions pertaining to the establishment, modification, or removal of governmental institutions and organizations.
   2. The department of taxation will consist of 3 assembly appointed representatives from each state. The department of taxation is tasked with the taxation of the people. It will collect and handle any taxes from citizens of the nation.
   3. The department of treasury and finance will consist of 3 assembly appointed representatives from each state. The department of treasury and finance is tasked with handling the budget and revenue of the nation and its individual states. It will handle all monetary assets in the possession of the governate.
6. The legislative senate (LS, or senate) is a branch of the governate with the duty of upholding liberty, justice, and order through laws and regulations.
   1. Legislation may regulate only: the behaviors and/or actions of citizens; or the production and/or use of inanimate objects, materials, or non-material utilities.
   2. The legislative assembly will consist of a governate appointed chairman, and 3 members from each state who will be selected through the representative assembly. It will be tasked with the nomination and election of representatives to and removal from the seats within other departments of the senate. It will also vote on actions pertaining to the establishment, modification, or removal of laws and regulations.
   3. The department of justice will consist of 3 assembly appointed representatives from each state. The department of justice is tasked with the archival and enforcement of laws.
   4. The department of regulation will consist of 3 assembly appointed representatives from each state. The department of regulation is tasked with the archival and enforcement of regulations.

## Section B: The establishment of governance

1. The government will be represented by the party-elect.
2. In the event the position of party-elect is not held, a new party will be elected to the party-elect by the populous.
3. In the event that the requirements of a governmental department are not satisfied and the population could plausibly fill the requirements of said department, the party-elect will appoint representatives or members by a majority vote from the party-elect.
4. If at any point the allocation of state seats shifts in favor of another party, they will be appointed as the new party-elect.
5. A representative may not realign themselves with another party than the one they currently represent at any point while occupying a seat in the governate.
6. All information possessed by the governate that, if released, would definitely not harm public safety must be publicly available, and be provided upon request. Information released will have all personally identifiable information redacted.

## Section C: The establishment of statehood

1. The people of the state will vote for the party they wish to be represented by and these votes will be counted by the representative assembly. The 10 available seats in the representative assembly will be allocated according to percentage of votes each party receives. The party with the most seats held will then nominate a representative from their party to assume the role of state governor until the next election. Such is the process of a state election and it will be held in a four year cycle starting at the year 2020 CE, after which every four years an election will be held, unless specified otherwise by the constitution.
2. If, at any point, more than half of a state's populous vote to, a state election will be called immediately.
3. To be defined as a state a community must have at least 34 members and a central system of authority capable of self governance and enforcement of TSWR's laws.
4. States will be granted a one month grace period after entering into TSWR to adapt to and enforce the law as they wish.
5. After the initial one month grace period has finished, the duty of law enforcement will fall from the state to the IM and senate and all laws and regulations must be followed according to the procedures of TSWR as a whole. At this point the population of the state will be considered citizens.

## Section D: Definition of rights

1. As a citizen, one has the right to be free from discrimination based on one's: gender, race, religion, sexual orientation, disability, or creed.
2. As a citizen, one has the right to freely express any sentiment in a lawful manner.
3. As a citizen, one has the right to privacy in all matters where the law is not being broken, unless one explicitly requests or allows for the release of information.
